import os
import numpy as np
import json
import cv2
from tqdm import trange

def get_particle_dicts(img_dir, json_file):
    """This function loads the JSON file created with the annotator and converts it to
    the detectron2 metadata specifications. I used this for my first test with detectron2. 
    However, this information is still relevant for use with UNet. Basic meta data such 
    as file name, height and width is being extracted along with the annotated polygons.
    We use these polygons later. This could definitely be simplified since we only need 
    the latter for UNet.
    """
    # load the JSON file
    with open(json_file) as f:
        imgs_anns = json.load(f)

    dataset_dicts = []
    # loop through the entries in the JSON file
    for idx, v in enumerate(imgs_anns.values()):
        record = {}
        # add file_name, image_id, height and width information to the records
        filename = os.path.join(img_dir, v["filename"])
        height, width = cv2.imread(filename).shape[:2]

        record["file_name"] = filename
        record["image_id"] = idx
        record["height"] = height
        record["width"] = width

        annos = v["regions"]

        objs = []
        # one image can have multiple annotations, therefore this loop is needed
        for annotation in annos:
            # reformat the polygon information to fit the specifications
            anno = annotation["shape_attributes"]
            px = anno["all_points_x"]
            py = anno["all_points_y"]
            poly = [(x + 0.5, y + 0.5) for x, y in zip(px, py)]
            poly = [p for x in poly for p in x]

            region_attributes = "particle"

            # specify the category_id to match with the class.
            if "particle" in region_attributes:
                category_id = 0

            obj = {
                "segmentation": [poly]
            }
            objs.append(obj)
        record["annotations"] = objs
        dataset_dicts.append(record)

    return dataset_dicts

def main():
    
    # loading the vgg annotated dataset
    dataset_dicts = get_particle_dicts('data/original/imgs', 'data/annotation/particleanalysis_json.json')
    
    # generating a binary mask from the data
    for i in trange(len(dataset_dicts)):
        height, width = dataset_dicts[i]["height"], dataset_dicts[i]["width"]
        mask = np.zeros([height, width], dtype=np.int32)

        # convert annotated polygons into vgg annotations
        for j in range(len(dataset_dicts[i]['annotations'])):
            polygon = np.array(dataset_dicts[i]['annotations'][j]['segmentation'][0])
            polygon = polygon.reshape([polygon.shape[0]//2, 2]).astype(np.int32)

            pts = polygon.reshape((-1, 1, 2))
            mask = cv2.fillPoly(mask, [pts], 255)
        
        # saving masks as png
        cv2.imwrite(dataset_dicts[i]['file_name'].replace('imgs', 'masks').split('.')[0]+'.png', mask)
        # the unet expects gifs, so we convert them
        os.system(f"convert '{dataset_dicts[i]['file_name'].replace('imgs', 'masks').split('.')[0]+'.png'}' '{dataset_dicts[i]['file_name'].replace('imgs', 'masks').split('.')[0]+'_mask.gif'}'")
        os.system(f"rm '{dataset_dicts[i]['file_name'].replace('imgs', 'masks').split('.')[0]+'.png'}'")
        
    
if __name__ == "__main__":
    main()
