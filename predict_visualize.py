import os
import sys
import glob
import csv

import numpy as np
import torch
import torch.nn.functional as F
from PIL import Image, ImageOps
from torchvision import transforms
from scipy.stats import entropy
from pathlib import Path

from utils.data_loading import BasicDataset
from unet import UNet

import cv2
import matplotlib.pyplot as plt
import logging

logger = logging.getLogger(__name__)
global data_path


def predict_img(net, full_img, device, scale_factor=1, out_threshold=0.5):
    net.eval()
    img = torch.from_numpy(
        BasicDataset.preprocess(full_img, scale_factor, is_mask=False)
    )
    img = img.unsqueeze(0)
    img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        output = net(img)

        if net.n_classes > 1:
            probs = F.softmax(output, dim=1)[0]
        else:
            probs = torch.sigmoid(output)[0]

        tf = transforms.Compose(
            [
                transforms.ToPILImage(),
                transforms.Resize((full_img.size[1], full_img.size[0])),
                transforms.ToTensor(),
            ]
        )

        full_mask = tf(probs.cpu()).squeeze()

    if net.n_classes == 1:
        return (full_mask > out_threshold).numpy(), full_mask
    else:
        return (
            F.one_hot(full_mask.argmax(dim=0), net.n_classes).permute(2, 0, 1).numpy(),
            full_mask
        )


def mask_to_image(mask: np.ndarray):
    if mask.ndim == 2:
        return Image.fromarray((mask * 255).astype(np.uint8))
    elif mask.ndim == 3:
        return Image.fromarray(
            (np.argmax(mask, axis=0) * 255 / mask.shape[0]).astype(np.uint8)
        )


def predict_visualize(imgs, gt_masks, net, device, outpath, flag="train", active_learning=False):
    # itereate through the train dataset and predict those
    path_helper = "" if flag == "train" else "test"
    active_learning_dict = {"filename": [], "score": [], "entropy": [], "maskpath": []}
    for filename, gt_mask in zip(imgs, gt_masks):
        logger.info(f"Predicting image {filename} ...")
        same_mask_path = os.path.join(
            os.path.dirname(gt_mask),
            os.path.basename(filename).replace(".jpg", "_mask.png"),
        )
        logger.info(f"same_mask_path: {same_mask_path}")
        img = Image.open(filename).convert("L")
        logger.info(f'shape of img: {img.size}')
        gt_mask = Image.open(same_mask_path).convert("L")
        logger.info(f'shape of gt_mask: {gt_mask.size}')
        pred_mask, probs = predict_img(
            net=net, full_img=img, scale_factor=0.35, out_threshold=0.1, device=device

        )
        logger.warning(f'shape of pred_mask: {pred_mask.shape}')
        logger.warning(f'shape of probs: {probs.shape}, type: {type(probs)}')
        score_entropy = entropy(probs.cpu().flatten().squeeze().numpy())
        score= np.sum(np.power(pred_mask - probs.cpu().numpy(), 2))

        active_learning_dict["filename"].append(filename)
        active_learning_dict["score"].append(score)
        active_learning_dict["entropy"].append(score_entropy)
        active_learning_dict["maskpath"].append(same_mask_path)
        out_filename = os.path.join(
            f"{outpath}",
            "predictions",
            path_helper,
            os.path.basename(filename.replace(".jpg", "_mask.png")),
        )
        out_pob_filename = out_filename.replace("_mask.png", "_prob.png")
        # make sure probs has shape (H, W)
        probs = mask_to_image(probs.cpu().numpy())
        logger.warning(f'shape of probs: {probs.size} after mask_to_image')
        logger.warning(f' type of probs: {type(probs)}')
        probs.save(out_pob_filename)

        if not os.path.exists(os.path.join(f"{outpath}", "predictions", path_helper)):
            Path(os.path.join(f"{outpath}", "predictions", path_helper)).mkdir(parents=True, exist_ok=True)
            logger.info(f"created {outpath}/predictions/{path_helper} dir")

        result = mask_to_image(pred_mask)

        result = Image.fromarray(255 - np.array(result) * 2)
        # change 0 to 255 and 255 to 0
        result = ImageOps.invert(result)
        result.save(out_filename)
        pred_mask = Image.open(out_filename).convert("L")
        # pred_mask = np.array(pred_mask) // 255
        pred_mask = np.array(pred_mask)
        # find contours of masks for plotting (we only want the contours for visualisation)
        contours_gt, _ = cv2.findContours(
            np.array(gt_mask), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
        )
        contours_pred, _ = cv2.findContours(
            pred_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
        )
        # draw particle contours
        img_gt = cv2.cvtColor(np.array(img), cv2.COLOR_GRAY2RGB)
        img_pred = cv2.cvtColor(np.array(img), cv2.COLOR_GRAY2RGB)
        cells_gt = cv2.drawContours(img_gt, contours_gt, -1, (0, 255, 0), 2)
        cells_pred = cv2.drawContours(img_pred, contours_pred, -1, (0, 255, 255), 2)
        # plot and save
        f, axarr = plt.subplots(1, 2)
        axarr[0].imshow(img_gt)
        axarr[0].axis("off")
        axarr[0].set_title("Groundtruth")
        axarr[1].imshow(img_pred)
        axarr[1].axis("off")
        axarr[1].set_title("Prediction")
        f.tight_layout()
        if not os.path.exists(os.path.join(f"{outpath}", "plots", "test")):
            Path(os.path.join(f"{outpath}", "plots", "test")).mkdir(parents=True, exist_ok=True)
            logger.info(f"created {outpath}/plots/test dir")
        plt.savefig(
            os.path.join(f"{outpath}", "plots", path_helper, os.path.basename(filename)),
            bbox_inches="tight",
            dpi=300,
        )
        plt.close()
    if not active_learning:
        return
    with open(
        os.path.join(f"{outpath}", "active_learning.csv"), "w"
    ) as f:
        writer = csv.DictWriter(f, active_learning_dict.keys())
        writer.writeheader()
        for i in range(len(active_learning_dict["filename"])):
            writer.writerow(
                {
                    "filename": active_learning_dict["filename"][i],
                    "score": active_learning_dict["score"][i],
                    "entropy": active_learning_dict["entropy"][i],
                    "maskpath": active_learning_dict["maskpath"][i],
                }
            )


def main():
    model = sys.argv[1]
    data_path = sys.argv[2]
    active_learning = sys.argv[3]
    outpath = sys.argv[4]
    net = UNet(n_channels=1, n_classes=2)
    if not os.path.exists(os.path.join(outpath, "predictions")):
        Path(os.path.join(outpath, "predictions")).mkdir(parents=True, exist_ok=True)

    if not os.path.exists(os.path.join(outpath, "predictions", "test")):
        Path(os.path.join(outpath, "predictions", "test")).mkdir(parents=True, exist_ok=True)

    if not os.path.exists(os.path.join(outpath, "plots")):
        Path(os.path.join(outpath, "plots")).mkdir(parents=True, exist_ok=True)
        logger.info(f"created {outpath}/plots/test dir")

    # get a list of all images and masks for test and train
    imgs = glob.glob(f"{data_path}/imgs/*.jpg")
    if len(imgs) == 0:
        # pool case
        imgs = glob.glob(f"{data_path}/pool/imgs/*.jpg")
    imgs.sort()
    masks = glob.glob(f"{data_path}/masks/*_mask*")
    if len(masks) == 0:
        # pool case
        masks = glob.glob(f"{data_path}/pool/masks/*_mask*")
    masks.sort()
    # do the same for the test images and masks
    imgs_test = glob.glob(f"{data_path}/test/imgs/*.jpg")
    imgs_test.sort()
    masks_test = glob.glob(f"{data_path}/test/masks/*_mask*")
    masks_test.sort()

    # load model from specified checkpoint
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    logger.info(f"Using device {device}")
    net.to(device=device)
    if os.path.exists(model):
        net.load_state_dict(torch.load(model, map_location=device))
        logger.info("Model loaded!")

    # predict and visualize the train and test images
    logger.info(f' imgs: {imgs}')
    logger.info(f'irst masks: {masks}')
    predict_visualize(imgs, masks, net, device, outpath, "train")
    logger.info('predict on test data')
    logger.info(f'first imgs_test: {imgs_test[0]}')
    predict_visualize(imgs_test, masks_test, net, device, outpath, "test")
    if active_learning:
        logger.info("Active learning enabled!")
        pool_imgs = glob.glob(f"{data_path}/pool/imgs/*.jpg")
        logger.info(f"pool_imgs: {pool_imgs}")
        pool_imgs.sort()
        pool_masks = glob.glob(f"{data_path}/pool/masks/*_mask*")
        pool_masks.sort()
        predict_visualize(pool_imgs, pool_masks, net, device, outpath, "train", active_learning=True)

    logger.info(f"Visualisation done! Saved at {data_path}/plots/")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    main()
