# Introduction

This repo explains how to run the activle learning experiments in our paper "How to estimate confluence, lazily".



## Quick start

1. Install dependencies
I recommend working in a virtual environment to avoid dependency conflicts
```bash
python -m venv unet-env
source unet-env/bin/activate
pip install -r requirements.txt
```

2. Download the data
You can find the processed data [here](https://cloud.scadsai.uni-leipzig.de/index.php/s/yrm6CmJMAJXgizT)
If you cannot access the link, please write me an e-mail: joas@informatik.uni-leipzig.de


## Activle Learning
### Active Learning theory

We use active learning (AL) to decide which images to label. Imagine you have a large data set of cell images an only limited budget to label. So it is not feasible to annoated the whole dataset. In this case you could just randomly selct X images and label them. Alternatively, you could use active learning, which will probably select a better training set.
The process works as follows:
- select an initial set of images to label (can be as small as 1 image for training)
- train a model on this images
- run inference on all unlabeled images with this model
- obtain an uncertainty score of how well the model can detect cells on each image
  - can be entropy of the output probabilites (High and low probabilites indicate mode is sure where we have cells, uniform probabilites indicate the model is unsure)
- rank the unlabeled images by the uncertainty
- label the X next images (can be as small as 1 image)
repeat process until labeling budget is reached (or goal accuracy)

### Active Learning implementation
In our case we've already labeled all images that we want to use. We want to mimic the AL process and compare the model perfomances on models trained with images selected by AL and randomly selected ones.

To run this experiment you need to the the following:
1. create a `pool` directory next to the `imgs` directory
  1.1 Create the directories `pool/imgs` and `pool/masks` (this should be already there)
2. move all images from `imgs` to `pool/imgs` and all masks form `masks` to `pool/masks`.
3. prepare the `al_config.toml` file. The following entries are to modifie:
```
[ARGS]
DATA_PATH = "LC_GENERALPREP_data" # Path to the data where the train, test and pool layers are located
STEP_SIZE = 1 # Number of images to be annotated in each AL iteration
INITIAL_TRAINING_SET = [] # List of images to be annotated in the first AL iteration, leave empty for random selection
INITIAL_TRAINING_SET_SIZE = 2 # only relevant if INITIAL_TRAINING_SET is empty, number of images to be randomly selected for the first AL iteration
STOPPING_CRITERION = 5 # Number of AL steps
RUN_ID = "LC_GENERALPREP_res" # Name of the run
SUBPROCESS_CALL = "python train.py --scale 0.5 --batch-size 1 --epochs 1 --learning-rate 0.0001 --run-id LC_GENERALPREP --output-dir"
PREDICT_CALL = "python predict_visualize.py"
USECASE = "al" # random or al (active learning)
```
6. run the `active_learning.py`:

```python active_learning.py al_config.toml```

Note on uncertainty scores:
The detectron2 model outputs a vector of confidence scores for each detected instance [see here](https://detectron2.readthedocs.io/en/latest/tutorials/models.html?highlight=scores#model-output-format)
. We use the average of all scores as a metric for uncertainty for detectron2.

For SAM and Unet, we use the entropy of the probability map output of the models and a custom score:
The custom score is calculated as follows:
1. Obtain probability map from model output (width n_channel x height dimensional tensor)
2. Select a threshold for cell detection e.g probability > 0.5
3. Obtain binary mask with threshold and probability map
4. Calculate the pairwise difference between binary mask and the probabilites
5. Square the difference and build the sum. The idea is that a low difference beteen binary mask and probability indicates low uncertainty about the output.

```
#### Activle Learning HPC
To run all experiments in paralel,on a HPC cluster, we can use the bash script `run_al_para.sh`. Our specific configurations were build with a Google sheets File can be found [here](https://docs.google.com/spreadsheets/d/17VrC_gHNiUvYN1dzoHK_zxtUer33ZLIoTW-RQTgJfTc/edit?usp=sharing) (sheet externalParallel, column I "COMMAND")


### Non-active learning
For the full training, we can use the script `run.sh` and fill the config file beforehand.
### Network Architecture

Original paper by Olaf Ronneberger, Philipp Fischer, Thomas Brox:

[U-Net: Convolutional Networks for Biomedical Image Segmentation](https://arxiv.org/abs/1505.04597)

![network architecture](https://i.imgur.com/jeDVpqF.png)




### Acknowledgements
This repo was originally created for particle segmentation by Raphael Hildebrand. We adjusted his implementation for cell segmentation of microscopy images.

The original implementation was based on [U-Net](https://arxiv.org/abs/1505.04597) in PyTorch for Segmentation of Particles in Electron Microscope Scans of Catalysts

Based on: https://github.com/milesial/Pytorch-UNet
