import sys
import os
from pycocotools.coco import COCO
import logging
from PIL import Image, ImageDraw
import numpy as np

if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    img_dir = sys.argv[1]  # path to coco.json file
    outpath = img_dir.replace('imgs', 'masks')
    # img_dir = '../confluence/cell_data/train/'

    coco = COCO(os.path.join(img_dir, 'annotations.json'))
    ids = [coco.imgs[k]['id']
           for k in coco.imgs.keys()]  # get list of image ids
    logger.info('Started to create masks from coco.json')
    logger.info('Using this file: {annotation_path}')
    # SAVE MASK ONLY ----------------------------------------------------------
    confluence_dict = {}
    for img_id in ids:
        img = coco.imgs[img_id]
        file_name = img['file_name']
        logger.info(f'creating mask for {file_name}')
        cat_ids = coco.getCatIds()
        anns_ids = coco.getAnnIds(imgIds=img['id'],
                                  catIds=cat_ids,
                                  iscrowd=None)
        anns = coco.loadAnns(anns_ids)
        img_out = Image.new("L", [img['width'], img['height']])
        img_out1 = ImageDraw.Draw(img_out)
        org_image = Image.open(os.path.join(
            img_dir, file_name))
        overlay_out = ImageDraw.Draw(org_image, 'RGBA')
        for i in range(len(anns)):
            segs = anns[i]['segmentation'][0]
            if len(segs) < 4:
                logger.warning(f'skipping polygon for ann {i} in file {file_name}, because there are less than 2 points')
                continue
            img_out1.polygon(segs, fill="white", outline="white")
            overlay_out.polygon(segs, fill=(
                21, 239, 116, 27), outline='#B7FF00')

    # give mask image the same filename as original except extension
        if not os.path.exists(os.path.join(outpath)):
            logger.info(f'creatind output dir: {outpath}')
            os.mkdir(os.path.join(outpath))
        if not os.path.exists(os.path.join(outpath, 'overlay')):
            os.mkdir(os.path.join(outpath, 'overlay'))
        outfile = os.path.join(outpath, file_name.split('.')[
                               0] + '_mask' + '.png')
        overlay_file = os.path.join(outpath, 'overlay', file_name.split('.')[
                                    0] + '_overlay' + '.png')
        org_image.save(overlay_file)
        img_out.save(outfile)
        logger.info(f'saved mask to {outfile}')
        out_ar = np.array(img_out)
        np.unique(out_ar, return_counts=True)
        confluence = (out_ar == 255).sum() / \
            (out_ar.shape[0] * out_ar.shape[1])
        confluence_dict[file_name] = (confluence, 'joas')
        with open('confluence.csv', 'w') as f:
            for key in confluence_dict.keys():
                f.write("%s,%s\n" % (key, confluence_dict[key]))
