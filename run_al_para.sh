#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --time=5:00:00
#SBATCH --gres=gpu:1
#SBATCH --mem=256G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=clara
#ml libGLU
export LD_LIBRARY_PATH=/usr/local/cuda-10.2/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}


cd 'development/confluence-unet'
source unet-env/bin/activate

ml CUDA/11.7.0
ml cuDNN/8.4.1.50-CUDA-11.7.0
ml Python/3.10.8


echo 'check CUDA'
python3 -c 'import torch; from torch.utils.cpp_extension import CUDA_HOME; print(torch.cuda.is_available(), CUDA_HOME)'


python active_learning.py "--configfile="$1 "--metarun="$2 "--num-exp=1" "--randomrun="$3 "--para" $4
echo "finished script"
# get name of %1 without extension
name=$(basename $1 .toml)
echo $name
# create timestamped folder of name_timestamp
#timestamp=$(date +%Y-%m-%d_%H-%M-%S)
#mkdir -p "results/$name-$timestamp"
# move all files with name to folder
#mv $name/* "results/$name-$timestamp"
#rm -r $name
# copy config file to output folder
#cp $1 "results/$name-$timestamp"
#echo moved output to "results/$name-$timestamp"
echo "finished job"
