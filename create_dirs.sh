#!/bin/bash
# Create directories for the new user
mkdir data
mkdir data/annotations
mkdir data/imgs
mkdir data/masks
mkdir data/original
mkdir data/original/imgs
mkdir data/original/masks
mkdir data/test
mkdir data/test/imgs
mkdir data/test/masks
