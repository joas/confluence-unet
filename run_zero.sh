#!/bin/bash
#SBATCH --job-name=unet_oshot
#SBATCH --output=/home/sc.uni-leipzig.de/me792rqay/development/confluence-unet/unet-0shot_slurm_%a_%j.out
#SBATCH --error=/home/sc.uni-leipzig.de/me792rqay/development/confluence-unet/unet-0shot_slurm_%a_%j.err
#SBATCH --mail-user=joas@informatik.uni-leipzig.de
#SBATCH --gres=gpu:v100:1
#SBATCH --mem=64G
#SBATCH --mail-type=ALL
#SBATCH --time=3:00:00
#SBATCH --nodes=1
#SBATCH --partition=clara

echo " change dir to RUNPATH"
RUNPATH=/home/sc.uni-leipzig.de/me792rqay/development/confluence-unet/
datapath=$1
outpath=$2
gtpath=$3
maskpath=$4
mask_ext=$5
train=$6
cd $RUNPATH
echo $PWD
source /home/sc.uni-leipzig.de/me792rqay/development/confluence-unet/unet-env/bin/activate
echo "activated venv"
ml libGLU

ml CUDA/11.7.0
ml cuDNN/8.4.1.50-CUDA-11.7.0
export LD_LIBRARY_PATH=/usr/local/cuda-10.2/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

if [ $train -eq 1 ]
then
    echo "predict and visualize"
    python predict_visualize.py ${outpath}/checkpoints/model_final.pth ${datapath} 0 $outpath
fi
echo 'starting eval script'
python evaluate.py --gt-path=$gtpath --gt-ext=$mask_ext --pred-path=$maskpath --pred-ext=$mask_ext --run-id=$outpath

