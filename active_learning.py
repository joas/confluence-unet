""" This is the main script for the active learning pipeline. It should be
univerally applicable to all our models. The script defines the active learning
step size and the initial training set. Then it calls the corresponding model
and trains it with the inital subset. The model will provide a csv file with
the predictions on the unknown data. The script will then select the next batch
of images based on the uncertainty score that we get from the model. The script
then updates the training set with the new batch and repeats the process until
the stopping criterion is met. In parallel, the script will also train a model
based on random sampling (matching the active learning step size)

his script will create a direcotry for each active learning step, containing
the input data, the model, the predictions, and the uncertainty scores. It will
also create a log file that keeps track of the model performance and the
training set size.

Later this data can be used to create plots of the model performance and the
training set size and we can compare active learning to random sampling.
"""


# ------------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------------
import os
import sys
import munch
import toml
import random
from datetime import datetime
import time
import shutil
from shutil import ignore_patterns
import subprocess
import glob
import logging
import argparse
import pandas as pd


# ------------------------------------------------------------------------------
# GLOBAL VARIABLES (VALUES ARE PASSED BY TOML FILE)
# ------------------------------------------------------------------------------
global DATA_PATH
global TEMP_DATA_PATH
global STEP_SIZE
global INITIAL_TRAINING_SET
global INITIAL_TRAINING_SET_SIZE
global STOPPING_CRITERION
global RUN_ID
global SUBPROCESS_CALL
global USECASE
global RESUME
global logger


# ------------------------------------------------------------------------------
# FUNCTIONS
# ------------------------------------------------------------------------------

def remove_folder(folder_path):
    try:
        shutil.rmtree(folder_path)
        logger.info(f"Folder '{folder_path}' successfully removed.")
    except OSError as e:
        logger.info(f"Error: {folder_path} : {e.strerror}")




def get_inital_training_set():
    """takes the MASTER_ANNOTATION_FILE and the INITIAL_TRAINING_SET and
    creates a new coco.json file with annotations only for the images in the
    INITIAL_TRAINING_SET
    """
    filenames = []

    if len(INITIAL_TRAINING_SET) == 0:
        logger.info("No initial training set provided. Using random sampling.")
        # select random ids
        pool_imgs = os.listdir(os.path.join(TEMP_DATA_PATH, "pool", "imgs"))
        logger.info(f"pool_imgs: {pool_imgs}")
        starting_imgs = random.sample(pool_imgs, INITIAL_TRAINING_SET_SIZE)
        # get masks
        starting_masks = [
            img.replace(".jpg", "") + "_mask.png" for img in starting_imgs
        ]
        logger.info(f"starting masks: {starting_masks}")
        # move images from pool to train
        for img in starting_imgs:
            shutil.move(
                os.path.join(TEMP_DATA_PATH, "pool", "imgs", img),
                os.path.join(TEMP_DATA_PATH, "imgs", img),
            )
            filenames.append(img)

        for mask in starting_masks:
            shutil.move(
                os.path.join(TEMP_DATA_PATH, "pool", "masks", mask),
                os.path.join(TEMP_DATA_PATH, "masks", mask),
            )
    else:
        # get initial masks
        logger.info(f"useing initial training set: {INITIAL_TRAINING_SET}")
        initial_mask = [
            img.replace(".jpg", "") + "_mask.png" for img in INITIAL_TRAINING_SET
        ]
        logger.info(f"inital mask: {initial_mask}")
        for img in INITIAL_TRAINING_SET:
            logger.info(f"moving {img} from pool to train")
            shutil.move(
                os.path.join(TEMP_DATA_PATH, "pool", "imgs", img),
                os.path.join(TEMP_DATA_PATH, "imgs", img),
            )
            filenames.append(img)
        for mask in initial_mask:
            shutil.move(
                os.path.join(TEMP_DATA_PATH, "pool", "masks", mask),
                os.path.join(TEMP_DATA_PATH, "masks", mask),
            )

    if not os.path.exists(os.path.join(RESULT_DIR_BASE, "AL_LOGGING")):
        logger.info("Creating logging directory")
        os.makedirs(os.path.join(RESULT_DIR_BASE, "AL_LOGGING"))
    outfile = os.path.join(RESULT_DIR_BASE, "AL_LOGGING", "step_0.txt")
    # write filenames to outfile
    with open(outfile, "w") as f:
        for img in filenames:
            f.write(img + "\n")


def get_next_batch(STEP):
    """This functions reads in a csv file with the uncertainty scores for each
    image and selects the next batch of images based on the uncertainty score.
    It also creates a new coco.json file with the annotations for the next
    training step in the active learning process.

    """
    filenames = []
    if USECASE == "al":
        logger.info(f"Running active learning for step {STEP}")
        scores_df = pd.read_csv(
            os.path.join(f"{RESULT_DIR_BASE}", f"step_{STEP}", "active_learning.csv")
        )
        # sort Descending, because the higher the entropy, the more uncertain the model is
        scores_df = scores_df.sort_values(by=["score"], ascending=False)
        logger.info(f"scores_df: {scores_df}")
        # check if we have enough images left in the pool
        if len(scores_df) < STEP_SIZE:
            logger.info(
                "We don't have enough images left in the pool. Using all remaining images."
            )
            next_images = scores_df["filename"].tolist()
            next_images = [os.path.basename(img) for img in next_images]
        else:
            # get next images by stepsize
            next_images = scores_df["filename"].iloc[:STEP_SIZE].tolist()
            next_images = [os.path.basename(img) for img in next_images]
        # next_masks = scores_df["maskpath"].iloc[:STEP_SIZE].tolist()
        # next_masks = [os.path.basename(mask) for mask in next_masks]
        next_masks = [img.replace(".jpg", "") + "_mask.png" for img in next_images]
        logger.info(f"We add the following images for the next batch: {next_images}")
        logger.info(f"Their masks are: {next_masks}")
        # move next images from pool to train
        for img in next_images:
            if os.path.exists(os.path.join(TEMP_DATA_PATH, "pool", "imgs", img)):
                shutil.move(
                    os.path.join(TEMP_DATA_PATH, "pool", "imgs", img),
                    os.path.join(TEMP_DATA_PATH, "imgs", img),
                )
                filenames.append(img)
        # update masks in pool
        for mask in next_masks:
            if os.path.exists(os.path.join(TEMP_DATA_PATH, "pool", "masks", mask)):
                shutil.move(
                    os.path.join(TEMP_DATA_PATH, "pool", "masks", mask),
                    os.path.join(TEMP_DATA_PATH, "masks", mask),
                )
    else:
        # if we use random sampling, we just select random images from the pool
        logger.info("We use random sampling for the next batch.")
        pool_images = os.listdir(os.path.join(TEMP_DATA_PATH, "pool", "imgs"))
        logger.info(f"pool_images: {pool_images}")
        random_next_images = random.sample(pool_images, STEP_SIZE)
        logger.info(f"random_next_images: {random_next_images}")
        # replace .jpg with .png and add _mask
        random_next_masks = [
            img.replace(".jpg", "") + "_mask.png" for img in random_next_images
        ]
        logger.info(
            f"We add the following images for the next batch: {random_next_images}"
        )
        logger.info(f"Their masks are: {random_next_masks}")
        # move next images from pool to train
        for img in random_next_images:
            if os.path.exists(os.path.join(TEMP_DATA_PATH, "pool", "imgs", img)):
                shutil.move(
                    os.path.join(TEMP_DATA_PATH, "pool", "imgs", img),
                    os.path.join(TEMP_DATA_PATH, "imgs", img),
                )
                filenames.append(img)
        # update masks in pool
        for mask in random_next_masks:
            if os.path.exists(os.path.join(TEMP_DATA_PATH, "pool", "masks", mask)):
                shutil.move(
                    os.path.join(TEMP_DATA_PATH, "pool", "masks", mask),
                    os.path.join(TEMP_DATA_PATH, "masks", mask),
                )

    if not RESUME:
        STEP += STEP_SIZE
    if RESUME and STEP == 0:
        STEP += STEP_SIZE


    previous_step = max(STEP - STEP_SIZE, 0)
    logger.info(f"previous_step: {previous_step}")

    with open(
        os.path.join(RESULT_DIR_BASE, "AL_LOGGING", f"step_{previous_step}.txt"),
        "r",
    ) as f:
        filenames_prev = f.readlines()
        filenames_prev = [x.strip() for x in filenames_prev if len(x.strip()) > 2]
    filenames = filenames_prev + filenames
    # write filenames to outfile
    with open(
        os.path.join(RESULT_DIR_BASE, "AL_LOGGING", f"step_{STEP}.txt"), "w"
    ) as f:
        for img in filenames:
            f.write(img + "\n")


def prepare_data(DATA_PATH):
    """function that creates temporary input dir with timestamp and moves all
    images and masks for train to pooly

    """
    # create human readable timestamp
    timestamp = time.time()
    date_time = datetime.fromtimestamp(timestamp)
    str_date_time = date_time.strftime("%d-%m-%Y-%H-%M-%S")
    if not os.path.exists(os.path.join(DATA_PATH, "pool", "imgs")):
        os.makedirs(os.path.join(DATA_PATH, "pool", "imgs"))
        logger.info(f'created {os.path.join(DATA_PATH, "pool", "imgs")}')
    if not os.path.exists(os.path.join(DATA_PATH, "pool", "masks")):
        os.makedirs(os.path.join(DATA_PATH, "pool", "masks"))
        logger.info(f'created {os.path.join(DATA_PATH, "pool", "masks")}')

    temp_in_path = os.path.join(DATA_PATH, f"temp-{USECASE}_{str_date_time}")
    logger.info(f"created {temp_in_path}")
    os.getcwd()

    shutil.copytree(
        DATA_PATH,
        temp_in_path,
        dirs_exist_ok=True,
        ignore=ignore_patterns("temp_*", "temp-*"),
    )
    logger.info(f"moving input data to {temp_in_path}")
    train_files = os.listdir(os.path.join(temp_in_path, "imgs"))
    train_masks = os.listdir(os.path.join(temp_in_path, "masks"))
    # move all images and masks to pool/imgs and pool/masks
    for img, mask in zip(train_files, train_masks):
        shutil.move(
            os.path.join(temp_in_path, "imgs", img),
            os.path.join(temp_in_path, "pool", "imgs", img),
        )
        shutil.move(
            os.path.join(temp_in_path, "masks", mask),
            os.path.join(temp_in_path, "pool", "masks", mask),
        )
    logger.info("moved imgs and masks to pool")
    return temp_in_path


def prepare_for_resume(laststep):
    logger.info("preparing for resume")
    last_completed_step = max(laststep - STEP_SIZE,0)
    logger.info(f"last completed step: {last_completed_step}")
    # get all files that were already used for training, according to the last completed step
    with open(
        os.path.join(
            RESULT_DIR_BASE, "AL_LOGGING", f"step_{last_completed_step}.txt"
        ),
        "r",
    ) as f:
        filenames = f.readlines()
    clean_filenames = [x.strip() for x in filenames if len(x) > 2]
    logger.info(f"len of already used files: {len(clean_filenames)}")
    # move all clean filenames from pool to train that are in clean_filenames,
    # since these are the images that were already used for training in the last step
    # since we starting the prepare data step at first, we need to move all images from pool to train
    # because all images are in pool at the beginning
    # do the same for masks
    for img in clean_filenames:
        mask = img.replace(".jpg", "") + "_mask.png"
        try:
            shutil.move(
                os.path.join(TEMP_DATA_PATH, "pool", "imgs", img),
                os.path.join(TEMP_DATA_PATH, "imgs", img),
            )
            shutil.move(
                os.path.join(TEMP_DATA_PATH, "pool", "masks", mask),
                os.path.join(TEMP_DATA_PATH, "masks", mask),
            )
        except Exception as e:
            # if the file is not in pool, we can ignore the error
            logger.info(e)
            logger.info(f"Could not move {img} from pool to train")
            pass
    logger.info(
        "moved all files from pool to train that were already used for training"
    )


def get_last_step():
    # check in results folder for all folder that start with run_id
    folders = glob.glob(os.path.join(RESULT_DIR_BASE, "step_*"))
    l = [int(os.path.basename(x).split("_")[-1]) for x in folders]
    last_step = max(l) if l else 0
    return last_step


def get_eval_metrics(i, random_str):
    """function that calls the evaluate.py script to get the evaluation metrics"""
    gt_path = os.path.join(DATA_PATH, "test", "masks")
    pred_path = os.path.join(
        RESULT_DIR_BASE, f"step_{random_str}{i}", "predictions", "test"
    )
    cur_run_id = os.path.join(RESULT_DIR_BASE, f"step_{random_str}{i}")
    call = [
        "python",
        "evaluate.py",
        f"--gt-path={gt_path}",
        "--gt-ext=png",
        f"--pred-path={pred_path}",
        "--pred-ext=png",
        f"--run-id={cur_run_id}",
    ]
    logger.info(f"calling evaluate.py with {call}")
    subprocess.call(call)


def active_learning():
    """main function that runs the active learning pipeline"""

    laststep = 0 if not RESUME else get_last_step()
    if RESUME:
        prepare_for_resume(laststep)
    logger.info(f"Last step: {laststep} and RESUME: {RESUME}")

    allpool_imgs = os.listdir(os.path.join(TEMP_DATA_PATH, "pool", "imgs"))
    # get total number of images
    len_total_ids = len(allpool_imgs)
    logger.info(f"Total number of images: {len_total_ids}")
    poolsize = len_total_ids - INITIAL_TRAINING_SET_SIZE
    logger.info(f"Poolsize: {poolsize} (images without initial training set)")
    range_limit = min(poolsize, STOPPING_CRITERION) + laststep + 1
    logger.info(f"Range limit: {range_limit}")
    if not RESUME:
        get_inital_training_set()
    if not os.path.exists(os.path.join(f"{RESULT_DIR_BASE}")):
        logger.info(f"Creating directory {RESULT_DIR_BASE}")
        os.makedirs(os.path.join(f"{RESULT_DIR_BASE}"))

    random_str = ""
    if USECASE == "random":
        random_str = "random_"
    for i in range(laststep, range_limit, STEP_SIZE):
        logger.info(f"Starting step {i}")
        # create cur_results dir
        results_dir = os.path.join(f"{RESULT_DIR_BASE}", f"step_{random_str}{i}")
        os.makedirs(results_dir, exist_ok=True)
        # here we need to call the actual training script of the model
        cur_call = SUBPROCESS_CALL.copy()
        cur_call.append("--output-dir")
        cur_call.append(results_dir)
        cur_call.append("--run-id")
        cur_call.append(TEMP_DATA_PATH)
        logger.info(f"Calling  \n: {cur_call}")
        subprocess.run(cur_call)
        # run predict processl
        logger.info("Running predict process")
        cur_predict_call = PREDICT_CALL.copy()
        checkpoint = os.path.join(results_dir, "checkpoints", "model_final.pth")
        cur_predict_call.append(checkpoint)
        cur_predict_call.append(TEMP_DATA_PATH)
        al = 1 if USECASE == "al" else 0
        cur_predict_call.append(str(al))
        cur_predict_call.append(f"{RESULT_DIR_BASE}/step_{random_str}{i}")
        logger.info(f"Calling  \n: {cur_predict_call}")
        subprocess.run(cur_predict_call)
        get_next_batch(i)
        logger.info(f"Finished step {i}, now getting evaluation metrics")
        get_eval_metrics(i, random_str)


if __name__ == "__main__":
    # use argparse to parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--configfile",
        dest="filepath",
        type=str,
        required=True,
        help="path to config file",
    )
    parser.add_argument(
        "--resume",
        dest="RESUME",
        action="store_true",
        help="flag to resume active learning pipeline",
    )
    parser.add_argument(
        "--metarun",
        dest="METARUN",
        type=str,
        default="R0",
        required=False,
        help="Name of the root dir for all AL Runs, goes from R0 to R<n>",
    )
    parser.add_argument(
        "--num-exp",
        dest="NUM_EXP",
        type=int,
        default=10,
        required=False,
        help="Number of how many times the active learning should be repeated"
    )
    parser.add_argument(
        "--para",
        dest="para",
        action="store_true",
        default=False,
        required=False,
        help=""
    )
    parser.add_argument(
        "--randomrun",
        dest="randomrun",
        type=str,
        default="",
        required=False,
        help=""
    )
    randomrun = parser.parse_args().randomrun
    PARALLEL = parser.parse_args().para

    NUM_EXP = parser.parse_args().NUM_EXP

    METARUN = parser.parse_args().METARUN

    filepath = parser.parse_args().filepath
    RESUME = parser.parse_args().RESUME

    logger = logging.getLogger(__name__)
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    # log stream handler to log to stdout
    handler = logging.StreamHandler(sys.stdout)
    logging.basicConfig(level=logging.INFO, format=log_fmt, handlers=[handler])

    CFG = munch.munchify(toml.load(filepath))
    DATA_PATH = CFG.ARGS.DATA_PATH
    USECASE = CFG.ARGS.USECASE
    STEP_SIZE = CFG.ARGS.STEP_SIZE
    INITIAL_TRAINING_SET = CFG.ARGS.INITIAL_TRAINING_SET
    INITIAL_TRAINING_SET_SIZE = CFG.ARGS.INITIAL_TRAINING_SET_SIZE
    STOPPING_CRITERION = int(CFG.ARGS.STOPPING_CRITERION)
    RUN_ID_BASE = CFG.ARGS.RUN_ID
    SUBPROCESS_CALL = CFG.ARGS.SUBPROCESS_CALL
    SUBPROCESS_CALL = SUBPROCESS_CALL.split(" ")
    PREDICT_CALL = CFG.ARGS.PREDICT_CALL
    PREDICT_CALL = PREDICT_CALL.split(" ")
    TEMP_DATA_PATH = prepare_data(DATA_PATH)

    last_run = 0
    if RESUME:
        folders = glob.glob(
            os.path.join(
                "../confluence-results/temp-results", RUN_ID_BASE, f"{RUN_ID_BASE}_*"
            )
        )
        l = [int(os.path.basename(x).split("_")[-1]) for x in folders]
        last_run = max(l) if l else 0

    for i in range(last_run, NUM_EXP):
        RUN_ID = f"{RUN_ID_BASE}_{i}"
        if PARALLEL:
            RUN_ID = f"{RUN_ID_BASE}_{randomrun}"

        RESULT_DIR_BASE = os.path.join(
            "../confluence-results/temp-results/", RUN_ID_BASE, f"{RUN_ID}"
        )
        logger.info(f"starting with {RUN_ID}")
        TEMP_DATA_PATH = prepare_data(DATA_PATH)
        # find position of --root-dir in SUBPROCESS_CALL
        active_learning()
        RESUME = False
        
        project_path = os.path.join("/projects/p_scads_saxocell/")
        if not os.path.exists(project_path):
            logger.info("Leipzig case")
            continue # LEIPZIG CASE
        helper = RESULT_DIR_BASE.replace("../","")
        project_res = os.path.join(project_path,helper)
        try:
            shutil.copytree(RESULT_DIR_BASE, project_res)
        except Exception as e:
            logger.warning(f'could not copy files to {project_res}')
            logger.warning(e)
            pass
        if PARALLEL:
            break

    timestamp = time.time()
    # create human readable timestamp
    date_time = datetime.fromtimestamp(timestamp)
    str_date_time = date_time.strftime("%d-%m-%Y-%H-%M-%S")

    final_res_path = os.path.join(
        "../confluence-results/final-results/",
        METARUN,
        f"{RUN_ID_BASE}_{str_date_time}",
    )
    if not os.path.exists(final_res_path):
        os.makedirs(final_res_path, exist_ok=True)

    shutil.move(
        os.path.join("../confluence-results/temp-results/", RUN_ID_BASE), final_res_path
    )
    logger.info("moving slurmlogs to result dir")
    move_logs = [
        "mv",
        f"slurm_{RUN_ID_BASE}*",
        "../confluence-results/final-results/{METARUN}/{RUN_ID_BASE}_{str_date_time}",
    ]

    subprocess.run(move_logs)
    logger.info(
        f"Moved {os.path.join('../confluence-results/temp-results/',RUN_ID_BASE)}/* to {final_res_path}"
    )
    df = pd.read_csv(
        os.path.join(
            "..", "confluence-results", "final-results", METARUN, "respaths.csv"
        ),
        index_col=0
    )
    df["path"][RUN_ID_BASE] = str(os.path.join(final_res_path, RUN_ID_BASE))
    df.to_csv(
        os.path.join(
            "..", "confluence-results", "final-results", METARUN, "respaths.csv"
        )
    )

    remove_folder(TEMP_DATA_PATH)
    logger.info("Finished active learning pipeline")
