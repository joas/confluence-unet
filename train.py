import argparse
import logging
import pandas as pd
import sys
from pathlib import Path
import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torch.utils.data import DataLoader, random_split
from tqdm import tqdm
import os

from utils.data_loading import BasicDataset, CarvanaDataset
from utils.dice_score import dice_loss
from evaluateunet import evaluate
from unet import UNet


## DEBUGGING -----------------------------------------------------------------
global dir_checkpoint
global dir_img
global dir_mask

# batch_size = 1
# val_percent = 0.1
# img_scale =1
# dataset = CarvanaDataset(dir_img, dir_mask, img_scale)
# for batch in train_loader:
#     images = batch['image']
#     true_mask = batch['mask']
#     break

# images.shape
# images
# true_mask.shape
# train_loader.dataset[:]


def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True

def train_net(
    net,
    device,
    epochs: int = 5,
    batch_size: int = 1,
    learning_rate: float = 0.001,
    val_percent: float = 0.1,
    save_checkpoint: bool = True,
    img_scale: float = 0.5,
    amp: bool = False,
):
    # 1. Create dataset
    try:
        dataset = CarvanaDataset(dir_img, dir_mask, img_scale)
        test_dataset = CarvanaDataset(dir_test_img, dir_test_mask, img_scale)
    except (AssertionError, RuntimeError):
        dataset = BasicDataset(dir_img, dir_mask, img_scale)
        test_dataset = BasicDataset(dir_test_img, dir_test_mask, img_scale)

    n_val = int(len(test_dataset))
    n_train = len(dataset)

    # 3. Create data loaders
    loader_args = dict(batch_size=batch_size, num_workers=4, pin_memory=True)
    train_loader = DataLoader(dataset, shuffle=True, **loader_args)
    val_loader = DataLoader(test_dataset, shuffle=False, drop_last=True, **loader_args)

    logging.info(
        f"""Starting training:
        Epochs:          {epochs}
        Batch size:      {batch_size}
        Learning rate:   {learning_rate}
        Training size:   {n_train}
        Validation size: {n_val}
        Checkpoints:     {save_checkpoint}
        Device:          {device.type}
        Images scaling:  {img_scale}
        Mixed Precision: {amp}
    """
    )

    # 4. Set up the optimizer, the loss, the learning rate scheduler and the loss scaling for AMP
    optimizer = optim.RMSprop(
        net.parameters(), lr=learning_rate, weight_decay=1e-8, momentum=0.9
    )
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, "max", patience=2
    )  # goal: maximize Dice score
    grad_scaler = torch.cuda.amp.GradScaler(enabled=amp)
    criterion = nn.CrossEntropyLoss()
    global_step = 0
    loss_dict = {"val": [], "train": [], "epoch": []}
    # 5. Begin training
    for epoch in range(epochs):
        loss_dict["epoch"].append(epoch)
        net.train()
        epoch_loss = 0
        epoch_vloss = 0
        with tqdm(
            total=n_train, desc=f"Epoch {epoch + 1}/{epochs}", unit="img"
        ) as pbar:
            for batch in train_loader:
                images = batch["image"]
                true_masks = batch["mask"]

                assert images.shape[1] == net.n_channels, (
                    f"Network has been defined with {net.n_channels} input channels, "
                    f"but loaded images have {images.shape[1]} channels. Please check that "
                    "the images are loaded correctly."
                )

                images = images.to(device=device, dtype=torch.float32)
                true_masks = true_masks.to(device=device, dtype=torch.long)

                with torch.cuda.amp.autocast(enabled=amp):
                    masks_pred = net(images)
                    loss = criterion(masks_pred, true_masks) + dice_loss(
                        F.softmax(masks_pred, dim=1).float(),
                        F.one_hot(true_masks, net.n_classes)
                        .permute(0, 3, 1, 2)
                        .float(),
                        multiclass=True,
                    )
                    epoch_loss += loss.item()

                with torch.no_grad():
                    net.eval()
                    for batch in val_loader:
                        images = batch["image"]
                        true_masks = batch["mask"]

                        assert images.shape[1] == net.n_channels, (
                            f"Network has been defined with {net.n_channels} input channels, "
                            f"but loaded images have {images.shape[1]} channels. Please check that "
                            "the images are loaded correctly."
                        )

                        images = images.to(device=device, dtype=torch.float32)
                        true_masks = true_masks.to(
                            device=device, dtype=torch.long
                        )
                        epoch_vloss += criterion(masks_pred, true_masks) + dice_loss(
                            F.softmax(masks_pred, dim=1).float(),
                            F.one_hot(true_masks, net.n_classes).permute(
                                0, 3, 1, 2).float(), multiclass=True,
                        )


                optimizer.zero_grad(set_to_none=True)
                grad_scaler.scale(loss).backward()
                grad_scaler.step(optimizer)
                grad_scaler.update()

                pbar.update(images.shape[0])
                global_step += 1
                epoch_loss /= n_train
                epoch_vloss /= n_val
                pbar.set_postfix(**{"loss (batch)": epoch_loss})

                # Evaluation round
                division_step = n_train // (10 * batch_size)
                if division_step > 0:
                    if global_step % division_step == 0:
                        val_score = evaluate(net, val_loader, device)
                        scheduler.step(val_score)
        loss_dict["val"].append(epoch_vloss.item())
        loss_dict["train"].append(epoch_loss)

        if epoch % 100 == 0:
            Path(dir_checkpoint).mkdir(parents=True, exist_ok=True)
            torch.save(
                net.state_dict(), f"{dir_checkpoint}/checkpoint_epoch{epoch + 1}.pth"
            )
            logging.info(f"Checkpoint {epoch + 1} saved!")
    logging.info(f'len epochs: {len(loss_dict["epoch"])}')
    logging.info(f'len train: {len(loss_dict["train"])}')
    logging.info(f'len val: {len(loss_dict["val"])}')
    # cat items of loss_dict to numpy if not already

    import csv
    df = pd.DataFrame(loss_dict)
    df.to_csv(os.path.join(args.output_dir, "lossdf.csv"))
    # write loss_dict to csv with csv writer
    with open(os.path.join(args.output_dir, "loss.csv"), "w") as f:
        writer = csv.writer(f)
        writer.writerow(loss_dict.keys())
        writer.writerows(zip(*loss_dict.values()))

    torch.save(net.state_dict(), f"{dir_checkpoint}/model_final.pth")


def get_args():
    parser = argparse.ArgumentParser(
        description="Train the UNet on images and target masks"
    )
    parser.add_argument(
        "--epochs", "-e", metavar="E", type=int, default=5, help="Number of epochs"
    )
    parser.add_argument(
        "--batch-size",
        "-b",
        dest="batch_size",
        metavar="B",
        type=int,
        default=1,
        help="Batch size",
    )
    parser.add_argument(
        "--learning-rate",
        "-l",
        metavar="LR",
        type=float,
        default=0.00001,
        help="Learning rate",
        dest="lr",
    )
    parser.add_argument(
        "--load", "-f", type=str, default=False, help="Load model from a .pth file"
    )
    parser.add_argument(
        "--scale",
        "-s",
        type=float,
        default=0.5,
        help="Downscaling factor of the images",
    )
    parser.add_argument(
        "--validation",
        "-v",
        dest="val",
        type=float,
        default=10.0,
        help="Percent of the data that is used as validation (0-100)",
    )
    parser.add_argument(
        "--amp", action="store_true", default=False, help="Use mixed precision"
    )
    parser.add_argument(
        "--run-id, -r",
        dest="run_id",
        type=str,
        default="0",
        help="Run ID for Input data and results",
    )
    parser.add_argument(
        "--output-dir, -d",
        dest="output_dir",
        type=str,
        default="results",
        help="path to output directory",
    )

    return parser.parse_args()


if __name__ == "__main__":
    seed_everything(100)
    args = get_args()
    
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    logging.info(f"Using device {device}")

    # Change here to adapt to your data
    # n_channels=3 for RGB images, n_channels=1 for grayscale
    # n_classes is the number of probabilities you want to get per pixel -> in our case 2 (background vs particle)
    net = UNet(n_channels=1, n_classes=2, bilinear=True)

    logging.info(
        f"Network:\n"
        f"\t{net.n_channels} input channels\n"
        f"\t{net.n_classes} output channels (classes)\n"
        f'\t{"Bilinear" if net.bilinear else "Transposed conv"} upscaling'
    )
    dir_img = os.path.join(f"{args.run_id}", "imgs")
    dir_mask = os.path.join(f"{args.run_id}", "masks")
    dir_test_img = os.path.join(f"{args.run_id}", "test", "imgs")
    dir_test_mask = os.path.join(f"{args.run_id}", "test", "masks")
    dir_checkpoint = os.path.join(args.output_dir, "checkpoints")
    if args.load:
        net.load_state_dict(torch.load(args.load, map_location=device))
        logging.info(f"Model loaded from {args.load}")

    net.to(device=device)
    try:
        train_net(
            net=net,
            epochs=args.epochs,
            batch_size=1,
            learning_rate=args.lr,
            device=device,
            img_scale=args.scale,
            val_percent=args.val / 100,
            amp=args.amp,
        )
    except KeyboardInterrupt:
        torch.save(net.state_dict(), "INTERRUPTED.pth")
        torch.save(net.state_dict(), f"{dir_checkpoint}/INTERRUPTED.pth")
        logging.info("Saved interrupt")
        sys.exit(0)
