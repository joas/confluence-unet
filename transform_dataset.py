import cv2
import numpy as np
import os
from scipy import ndimage
import imageio.v2 as imageio
import matplotlib.pyplot as plt
from tqdm import trange
import sys
global run_id

# function to randomly transform the data for better data diversity
# TODO: this should be done directly in torch (random augmentations) and would save a lot of storage
# -> probably also some training benefits
def transform(masks, imgs, num):
    for i in trange(len(masks)):
        # load images and masks
        og_m = imageio.imread(f"{run_id}_data/original/masks/"+masks[i])
        og_im = imageio.imread(f"{run_id}_data/original/imgs/"+imgs[i])
        og_shape = og_m.shape

        # save originals
        imageio.imwrite(f"{run_id}_data/masks/{masks[i]}", og_m)
        imageio.imwrite(f"{run_id}_data/imgs/{imgs[i]}", og_im)

        # flip mask
        mudf = np.flipud(og_m) # up-down flip
        mlrf = np.fliplr(og_m) # left-right flip
        mudlrf = np.flipud(mlrf) # left-right & up-down flip

        # flip img
        imudf = np.flipud(og_im) # up-down flip
        imlrf = np.fliplr(og_im) # left-right flip
        imudlrf = np.flipud(imlrf) # left-right & up-down flip

        # save results of flipping
        imageio.imwrite(f"{run_id}_data/masks/udf_{masks[i]}", mudf)
        imageio.imwrite(f"{run_id}_data/masks/lrf_{masks[i]}", mlrf)
        imageio.imwrite(f"{run_id}_data/masks/udlrf_{masks[i]}", mudlrf)

        imageio.imwrite(f"{run_id}_data/imgs/udf_{imgs[i]}", imudf)
        imageio.imwrite(f"{run_id}_data/imgs/lrf_{imgs[i]}", imlrf)
        imageio.imwrite(f"{run_id}_data/imgs/udlrf_{imgs[i]}", imudlrf)

        # random rotation for specified number `num` of times
        for j in range(num):
            m = np.copy(og_m)
            im = np.copy(og_im)
            new_name = np.random.randint(0,100000)

            # random rotation
            rot = np.random.randint(0,360) # get random rotation angle
            m = ndimage.rotate(m, rot, reshape=False) # rotate mask
            im = ndimage.rotate(im, rot, reshape=False) # rotate img

            # save rotated img and mask
            imageio.imwrite(f"{run_id}_data/masks/{new_name}_{masks[i]}", m)
            imageio.imwrite(f"{run_id}_data/imgs/{new_name}_{imgs[i]}", im)

            # flip rotated masks
            mudf = np.flipud(m) # up-down flip
            mlrf = np.fliplr(m) # left-right flip
            mudlrf = np.flipud(mlrf) # left-right & up-down flip

            # flip rotated imgs
            imudf = np.flipud(im) # up-down flip
            imlrf = np.fliplr(im) # left-right flip
            imudlrf = np.flipud(imlrf) # left-right & up-down flip

            # save flipped imgs/masks
            imageio.imwrite(f"{run_id}_data/masks/udf_{new_name}_{masks[i]}", mudf)
            imageio.imwrite(f"{run_id}_data/masks/lrf_{new_name}_{masks[i]}", mlrf)
            imageio.imwrite(f"{run_id}_data/masks/udlrf_{new_name}_{masks[i]}", mudlrf)

            imageio.imwrite(f"{run_id}_data/imgs/udf_{new_name}_{imgs[i]}", imudf)
            imageio.imwrite(f"{run_id}_data/imgs/lrf_{new_name}_{imgs[i]}", imlrf)
            imageio.imwrite(f"{run_id}_data/imgs/udlrf_{new_name}_{imgs[i]}", imudlrf)

def main():
    print("Transforming orignal dataset...")
    run_id = sys.argv[1]
    # get list of orginal imgs and masks
    masks = os.listdir(f"{run_id}_data/original/masks")
    imgs = os.listdir(f"{run_id}_data/original/imgs")

    # filter for correct filetype
    masks = [m for m in masks if '.gif' in m]
    imgs = [im for im in imgs if '.png' in im]

    # sort so both lists are in the same order
    imgs.sort()
    masks.sort()

    # might take quite a while (ca. 30min on my machine)
    transform(masks=masks, imgs=imgs, num=20)

    print("Done!")

if __name__ == "__main__":
    main()
